import json
import sys
import threading

from flask import Flask, request, jsonify

from time import time
from hashlib import sha3_256
from random import getrandbits

import requests

MOQ_PORT = None
KOLKO_NULI = 6
RANDOM_BITS = 128
VIKA4I = []
DALAVERI = []
BLOCKS = []


class Block(object):
    def __init__(self, predniq_hash, dalaveri, dokazatelstvo):
        self.predniq_hash = predniq_hash
        self.dalaveri = [list(dalavera) for dalavera in dalaveri]
        self.dokazatelstvo = dokazatelstvo

    def hash(self):
        return sha3_256(json.dumps({
            "predniq_hash": self.predniq_hash,
            "dalaveri": self.dalaveri,
            "dokazatelstvo": self.dokazatelstvo,
        }).encode("utf-8")).hexdigest()

    def serializirai_se(self):
        return {
            "predniq_hash": self.predniq_hash,
            "dalaveri": self.dalaveri,
            "dokazatelstvo": self.dokazatelstvo,
            "hash": self.hash(),
        }

    def __repr__(self):
        return "<Блок {}>".format(self.dokazatelstvo)


def kopai():
    global DALAVERI
    start = time()
    trqbvat_mi = "0" * KOLKO_NULI
    print("КОПАЧ: трябват ми {} нули".format(trqbvat_mi))
    while True:
        m = sha3_256(str(getrandbits(RANDOM_BITS)).encode())
        dokazatelstvo = m.hexdigest()
        if dokazatelstvo.startswith(trqbvat_mi):
            print("КОПАЧ: намерих {} нули за {} време: {}".format(trqbvat_mi, time() - start, dokazatelstvo))
            if len(BLOCKS) == 0:
                predniq_hash = 0
            else:
                predniq_hash = BLOCKS[-1].hash()
            block = Block(predniq_hash, dalaveri=DALAVERI, dokazatelstvo=dokazatelstvo)

            DALAVERI = []
            BLOCKS.append(block)
            for vika4 in VIKA4I:
                requests.post("http://localhost:{}/nov_blok".format(vika4),
                              json=block.serializirai_se())


def tuka_sum(moq_port, ima_li_nqkoi=None):
    if ima_li_nqkoi:
        print("Да кажа че съм тука:", format(moq_port))
        r = requests.post("http://localhost:{}/regni_se".format(ima_li_nqkoi),
                          json={"vika4": moq_port})
        tehnite_vika4i = r.json()
        print("техните викачи: {}".format(tehnite_vika4i))
        for vika4 in tehnite_vika4i:
            if vika4 not in VIKA4I and vika4 != MOQ_PORT:
                print("Оу, разбрах че {} е тука".format(vika4))
                VIKA4I.append(vika4)
        print("Мойте викачи: {}".format(VIKA4I))


app = Flask(__name__)


@app.route("/regni_se", methods=["POST"])
def regni_se():
    vika4 = request.json.get("vika4")
    if vika4 not in VIKA4I:
        print("Нов викач {}".format(vika4))
        VIKA4I.append(vika4)
    for drug_vika4 in VIKA4I:
        if drug_vika4 != vika4:
            print("Да кажа на {} за {}".format(drug_vika4, vika4))
            requests.post("http://localhost:{}/doide_nov".format(drug_vika4),
                          json={"vika4": vika4})
    print("Мойте викачи: {}".format(VIKA4I))
    return jsonify(VIKA4I)


@app.route("/doide_nov", methods=["POST"])
def doide_nov():
    vika4 = request.json.get("vika4")
    if vika4 not in VIKA4I and vika4 != MOQ_PORT:
        print("Чух че дойде нов викач {}".format(vika4))
        VIKA4I.append(vika4)
    print("Мойте викачи: {}".format(VIKA4I))
    return jsonify(VIKA4I)


@app.route("/nova_dalavera", methods=["POST"])
def nova_dalavera():
    zarqda = request.json
    ot = zarqda.get("ot")
    do = zarqda.get("do")
    pari = zarqda.get("pari")
    dalavera_id = len(DALAVERI)
    zarqda.update({"dalavera_id": dalavera_id})
    dalaverata = (dalavera_id, ot, do, pari)
    if dalaverata in DALAVERI:
        return "тая вече сме я слушали: {}".format((ot, do, pari))
    DALAVERI.append(dalaverata)
    for vika4 in VIKA4I:
        print("Да кажа на {} за далаверата {}".format(vika4, dalaverata))
        requests.post("http://localhost:{}/kaji_za_dalaverata".format(vika4),
                      json=zarqda)

    print("Мойте далавери: {}".format(DALAVERI))
    return ""


@app.route("/kaji_za_dalaverata", methods=["POST"])
def kaji_za_dalaverata():
    zarqda = request.json
    dalavera_id = zarqda.get("dalavera_id")
    ot = zarqda.get("ot")
    do = zarqda.get("do")
    pari = zarqda.get("pari")
    dalaverata = (dalavera_id, ot, do, pari)
    if dalaverata in DALAVERI:
        return "тая вече сме я слушали: {}".format((ot, do, pari))
    DALAVERI.append(dalaverata)
    print("Мойте далавери: {}".format(DALAVERI))
    return ""


@app.route("/nov_blok", methods=["POST"])
def nov_blok():
    zarqda = request.json
    predniq_hash = zarqda.get("predniq_hash")
    dalaveri = zarqda.get("dalaveri")
    dokazatelstvo = zarqda.get("dokazatelstvo")
    nov_blok = Block(
        predniq_hash=predniq_hash,
        dalaveri=dalaveri,
        dokazatelstvo=dokazatelstvo,
    )
    if nov_blok in BLOCKS:
        return "тоя вече сме я слушали: {}".format(nov_blok)
    BLOCKS.append(nov_blok)
    print("Мойте блокове: {}".format(BLOCKS))
    global DALAVERI
    DALAVERI = []
    return ""


if __name__ == "__main__":
    # block = Block(
    #     predniq_hash=0,
    #     dalaveri=[(0, '5001', '5002', '20')],
    #     dokazatelstvo=sha3_256(str(b'00').encode()).hexdigest()
    # )
    # print(block.serializirai_se())
    kopa4 = threading.Thread(target=kopai)
    kopa4.start()
    MOQ_PORT = sys.argv[1]
    ima_li_nqkoi = None
    if len(sys.argv) > 2:
        ima_li_nqkoi = sys.argv[2]
        VIKA4I.append(ima_li_nqkoi)
    tuka_sum(MOQ_PORT, ima_li_nqkoi)
    app.run(port=MOQ_PORT)
    kopa4.join()
