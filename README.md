# Qastro Pari
Блокчейн за всички пернишки копачи!

## Инсталирай

```
./INSTALL.sh
```

## Инструкции

`slu6a4.py` е основния сървър, който стартира копач и слуша за нови копачи и блокове. 

Активирай виртуалния енв (след като си инсталирал):

```
source venv/bin/activate
```

Първия слушач приема един аргумент порт, т.е. стартирай слушач на порт 5000:

```
python slu6a4.py 5000
```

Всеки следващ слушач приема собствен порт и порт на някой от предните слушачи:

```
python slu6a4.py 5001 5000
```

## За да симулираш далавера:

`napravi_dalavera.py` е симулатор на далавери, който приема 3 порта - кой слушач да нотифицираш, от-порт, до-порт и сума, примерно:

```
source venv/bin/activate
python napravi_dalavera.py 5000 5001 5002 26
```

Горното значи, че ще нотифицира слушач 5000 за далавера между 5001 и 5002 за 26 пари

## НЕ Е ГОТОВО:

Значи, златния път би трябвало да бачка (да пуснеш 2-3 слушачи, да си копаят блокове и да се слушат, НО:
- Никаква верификация на нищо включително дали слушачите имат достатъчно пари, кои далавери да се включат
- След копането не се дават пари на копача (а тва е цялата далавера)
- Нищо извън localhost няма да работи (и слава Рандома)
- Формата на пост рекуестовете
- Дали блоковете са реални (дали хашовете си съвпадат с предишния хаш - цялата сделка на блокЧЕЙНА)
- И да, въобще никаква верификация на нищо
