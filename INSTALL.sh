#!/usr/bin/env bash
sudo apt install -y python3-virtualenv python3-pip
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt